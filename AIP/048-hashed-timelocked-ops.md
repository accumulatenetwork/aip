| AIP   | Title                          | Status | Category   | Author                                                                      | Created  |
| ----- | ------------------------------ | ------ | ---------- | --------------------------------------------------------------------------- | -------- |
| 048   | Hashed time-locked operations  | Draft  | core       | Jay Smith \<jsmith@defidevs.io\><br>Ethan Reesor \<ethan.reesor@gmail.com\> | 20240718 |

# Summary

Implement hashed time-locked operations functionally equivalent to hashed time
locked contracts of other protocols such as BitCoin and Ethereum. The
implementation must be maximally interoperable with BitCoin and Ethereum HTLC
implementations.

# Motivation

We need atomic swaps to support trading value between Accumulate and other
ecosystems.  The ability to bring value in and out of Accumulate is important to
Accumulate stakeholders building any sort of product marketplace as there's
little point in a marketplace if value cannot be imported into the protocol to
buy the wares offered in Accumulate based marketplaces.

# Specification

* The transaction header includes an (optional) field containing the hash of a
  secret and an expiration time.
* If a two-phase transaction (such as SendTokens) is executed (successfully) and
  the hashlock field is set, the transaction produces a locked operation instead
  of the usual second phase.
* This locked operation is sent (synthetically) to the recipient.
* The locked operation remains pending until it expires, in which case it is
  reverted, or until a release message containing the revealed secret is
  processed, in which case the operation is executed.

# Implementation

TBD

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
