| AIP | Title                      | Status | Category | Created  | Authors                                                                             |
| --- | -------------------------- | ------ | -------- | -------- | ----------------------------------------------------------------------------------- |
| 003 | Rejections and abstentions | Draft  | Core     | 20230510 | Ethan Reesor \<ethan.reesor@gmail.com\><br />Dennis Bunfield \<bunfield@gmail.com\> |

# Summary

Implement additional signing mechanisms to allow users to construct advanced
signing scenarios. Specifically, add rejection votes, abstentions, and
associated thresholds.

# Motivation

- **Enhanced Security**: By introducing a reject transaction command, the
  multisig setup can provide an additional layer of security. It allows signers
  to actively reject transactions they consider suspicious or potentially
  fraudulent. This can help prevent unauthorized transactions from being
  executed, even if the required number of signatures is obtained.
- **Error Correction**: In some cases, errors or mistakes can occur during the
  transaction creation or signing process. With a reject transaction command,
  signers have the ability to rectify such errors by explicitly rejecting the
  erroneous transaction. This ensures that only accurate and valid transactions
  proceed, promoting transactional integrity.
- **Dispute Resolution**: In multisig scenarios where there may be disagreements
  or disputes among the signers regarding the validity or intent of a
  transaction, the reject transaction command can serve as a mechanism for
  resolving such conflicts. It allows dissenting signers to express their
  objection and prevent the transaction from being executed until the dispute is
  resolved.
- **Fraud Prevention**: The reject transaction command can act as a safeguard
  against fraudulent activities. If one of the signers detects any suspicious
  behavior or attempts at unauthorized transfers, they can reject the
  transaction to prevent financial loss or compromise of funds. This can be
  particularly useful in situations where one of the signers' keys or
  credentials may have been compromised.
- **Flexibility and Control**: Including a reject transaction command grants
  signers greater control and flexibility over the execution of transactions. It
  empowers them to actively participate in the decision-making process, rather
  than being solely reliant on predefined rules or automatic consensus. This can
  be beneficial in situations where signers have specific preferences, risk
  thresholds, or compliance requirements.
- **Compliance and Regulatory Requirements**: Some industries or jurisdictions
  may have specific compliance or regulatory requirements that demand explicit
  rejection capabilities for certain types of transactions. By incorporating a
  reject transaction command, the multisig setup can align with these
  requirements and ensure adherence to the applicable rules and regulations.

# Specification

The necessary definitions for the new vote/response types and thresholds were
added to the protocol prior to 1.0. However the thresholds can't be set and the
vote/response types are not respected.

- Enable rejection votes and abstention responses.
- Enable configuration of rejection and response thresholds.
- Update signature processing to respect the new vote/response types and
  thresholds.

# Implementation

- Add a new key page operation for the rejection threshold and for the response
  threshold.
- Update the signature processing code (pythonic pseudocode):

```python
def shouldSendVote(txn, book):
    # Check if the book is ready to vote
    vote = bookCanVote(txn, book)
    if vote is None:
        return

    # Remove references to data that may not be preserved
    for page in book.pages:
        page.clearSignatures()

    send(new AuthoritySignature(
        vote:      vote,
        txn:       txn,
        authority: book,
    ))

def bookCanVote(txn, book):
    # The book votes once a page is ready to vote
    for page in book.pages:
        vote = pageCanVote(txn, book, page)
        if vote is not None:
            return vote

    return None

def pageCanVote(txn, book, page):
    # Count the votes
    allVotes = 0
    voteCount = {}
    for sig in page.signaturesFor(txn):
        allVotes += 1
        voteCount[sig.vote] += 1

    # Check the response threshold
    if allVotes < page.responseThreshold:
        return None

    # Check the accept threshold
    if voteCount[VoteType.Accept] >= page.acceptThreshold:
        return VoteType.Accept

    # Check the reject threshold
    if voteCount[VoteType.Reject] >= page.rejectThreshold:
        return VoteType.Reject

    # If it is not possible to reach consensus, the authority abstains. If
    # the vote can be swung by every remaining (undecided) voter voting
    # together to accept or reject, it is still possible to reach
    # consensus. Thus if that is not possible for accept, or for reject,
    # consensus is unreachable.
    undecided = len(page.keys) - allVotes
    mayAccept = voteCount[VoteType.Accept] + undecided >= page.acceptThreshold
    mayReject = voteCount[VoteType.Reject] + undecided >= page.rejectThreshold
    if not (mayAccept or mayReject):
        return VoteType.Abstain

    return None
```

In reality the vote counting happens on a per delegation path basis, meaning
that each unique delegation path from A to B is counted separately. Additionally
if the rejectThreshold is unspecified, the corresponding check uses the
acceptThreshold instead. So if a page specifies accept threshold = M and does
not specify a reject threshold, M is used for both acceptances and rejections.
These details are omitted to make the pseudocode more readable.

# Background

**Signing logic MUST NOT depend on any records that may be pruned and MUST NOT
depend on cross-domain records.**

Accumulate is designed around a light-weight validator model. While it has not
been implemented yet, the intent is that validators aggressively prune the
database, pruning any records that are not necessary for validation of new
messages (signatures and transactions). The snapshot process has been aligned
with this - data that may be pruned is not preserved by a snapshot. **Depending
on data that may be pruned could lead to a consensus failure** between nodes
with the data and nodes without the data (e.g. nodes that have pruning enabled
or booted from a snapshot).

Accumulate is partitioned. If account A and account B belong to different
domains (different root ADIs), they may be resident on different partitions. If
they are resident on different partitions, account A has no reasonable mechanism
to access the records of account B. To avoid complicated code, and to avoid
surprising behavior dependent on whether accounts are resident on the same
partition, logic executed in the context of one domain is forbidden from
interacting with or in any way accessing records of a different domain.

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).