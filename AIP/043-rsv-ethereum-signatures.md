| AIP | Title                   | Status | Category | Author                                  | Created  |
| --- | ----------------------- | ------ | -------- | --------------------------------------- | -------- |
| 005 | RSV Ethereum Signatures | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20230831 |

# Summary

Expect Ethereum signatures to be encoded with RSV instead of DER.

# Motivation

Better compatibility with existing Ethereum tooling.

# Specification

- Moving forward, Ethereum signatures must be encoded with RSV.

# Implementation

- Add an activation version.
- When that version is activated, switch to decoding Ethereum signatures with
  RSV instead of with DER.

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).