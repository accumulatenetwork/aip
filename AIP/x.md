| AIP   | Title                                           | Status | Category | Author                              | Created  |
| ----- |------------------------------------------------ | ------ | -------- | ----------------------------------- |----------|
| X     | Standard, Process and Guidelines for Accumulate | Draft  | Meta     | Niels Klomp \<nklomp@sphereon.com\> | 20220214 |



# AIP-X

[AIP-X](/) is the initial proposal that describes the core framework,
conventions, contribution guidelines, and ecosystem of AIP.


# What Is an AIP?

An AIP defines a formal proposal for accepting and integrating new standards into Accumulate. It is  more or less required for parties building on top of Accumulate to adopt
these specifications, where they are applicable to their solutions, as it describes how Accumulate works.

An AIP itself is never about the development of software itself. It describes the interfaces, architectures, processes, code examples. Developers may choose to make their
software compatible with one or more Accumulate Improvement Proposals.


# Why Are They Important?

AIP provides an open forum for the community to collaborate on and accept new
standards for Accumulate. The Accumulate Protocol
is completely open source, and we rely on our community and committees to keep us pointed
in the right direction. Everybody can create any software running on top of the protocol.
As the protocol is public we encourage developers to develop their applications with
Improvement in mind, so that people can interact with users of your software, without
requiring use of your software and vice versa.


# Contributing

Create your submission in a markdown file following the guidelines defined in
[AIP-X](x.md) and the [AIP-Template](template.md)

To submit your specification, fork this repo and then submit a pull request to have
it reviewed and included for evaluation by the reviewers.


# Specification Categories

## Core

Standards that define the basic building blocks, technology, and conventions
that govern Accumulate standards.


## Infrastructure

Secondary infrastructure that is still part of the Accumulate core services, but
outside of the protocol implementation.


## Apps &amp; libraries

Standards that define application level functionality on top of Accumulate.


## Interface

Standards that define API architecture, conventions, and specs.


## Identities & key management

Standards that are related to identities on Accumulate, like native Accumulate identities, ADIs,
DIDs, and cryptographic key management/replacement.

## Wallets

Standards that govern hardware and software wallets.


## Explorers

Standards for explorers.


## APIs

Specifications for 2nd layer API tools, like Accumulate OpenAPI. This section is restricted to
dedicated APIs, and should not include specifications for other tools such as wallets
that only happen to have an API.


## Meta

Standards about AIP itself, processes, etc.


# Specification Statuses


## Work In Progress (WIP)

The specification's information is sufficient to be reviewed by the community. This
is essentially an "Intent to submit" an AIP. The community member(s)
submit a formatted pull request containing the preliminary version of the
proposed specification.

- If reviewed and denied, the specification may be revised and improved unless it is
  explicitly rejected
- If reviewed and approved it is assigned an AIP number and other metadata.
  The specification moves on to drafting.


## Draft

The specification is in the process of being revised. Follow-up pull requests will
be accepted to revise the specification until it is ready to go through the last
call process (explained below).


## Last Call

The specification is open to final evaluation by the community and the general
public.

- If the specification requires further changes, it reverts to drafting again.
- If the specification is approved then it will
  move onto final.

As AIP is a new standard, there is currently no maximum timeline set for this status.


## Final

The specification has been finalized and accepted by the community. The specification is
adorned with the final official prefix **Accumulate-Protocol**, replacing the former **AIP**
prefix. Errata may be formally submitted following this stage if required.


# Specification Workflow


# AIP Editors

For each new AIP, an editor will:

- Read the AIP to check if it is ready, sound and complete. The ideas must
  make technical sense, even if they don't seem likely to get to final status.
- The title should accurately describe the content.
- Check the AIP for language (spelling, grammar, sentence structure, etc.),
  markup (Github flavored Markdown), code style.

If the AIP isn't ready, the editor will send it back to the author for
revision, with specific instructions.

Once the AIP is ready for the repository, the AIP editor will:

- Assign a AIP number (generally the PR number or, if preferred by the
  author, the Issue # if there was discussion in the Issues section of this
repository about this AIP).
- Merge the corresponding pull request.
- Send a message back to the AIP author with the next step.

Many AIPs are written and maintained by developers with write access to the codebases
of the Accumulate Protocol or 3rd party applications. The AIP editors monitor AIP changes, and correct any
structure, grammar, spelling, or markup mistakes they see.




# Specification Structure

A [AIP Template](template) is supplied to begin writing your standard.


## Header

An informational header containing metadata about the AIP being submitted,
like so:

| AIP   | Title         | Status | Category | Author                             | Created    |
| ----- | ------------- | ------ | -------- | ---------------------------------- |------------|
| N     | Standard Name | Status | Category | Author Name \<author@example.com\> | 2022-02-03 |

Once accepted as a draft an editor will assign an official AIP number.


## Summary

"If you can't explain it simply, you don't understand it well enough." Provide
a simplified and layman-accessible explanation of the AIP.


## Motivation

Clearly explain what the existing problem is and how your AIP would
address the problem. AIP submissions without sufficient
motivation may be rejected outright. What motivated the design and why were
particular design decisions made?


## Specification

The technical specification should describe the syntax and semantics of any new
feature. The specification should be detailed enough to allow for different,
interoperable implementations.


## Implementation

As AIP is about specification, a software product is not necessarily the outcome of a AIP.
However, a reference implementation is always welcome.


## Copyright

The standard must have a copyright section that waives rights according to CC0. Please note that
this is only applicable to the proposal itself:

```
Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
```


# Inheritance & Dependencies

Accumulate Improvement Proposals can extend and inherit functionality and rules from others. The
`extends` \<AIP #> Keyword and Tag denotes feature inheritance on a feature
by feature basis. The author of the AIP must explain how and what is being
inherited, and if there are any changes being made. It is also possible to depend on another AIP. The
`depends-on` \<AIP #> Keyword and Tag denotes dependency on a feature
by feature basis.


# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
