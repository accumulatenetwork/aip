| AIP | Title                | Status | Category | Author                                  | Created  |
| --- | -------------------- | ------ | -------- | --------------------------------------- | -------- |
| 009 | Empty authority sets | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20230831 |

# Summary

Allow the authority set of a non-root account to be empty, in which case it
inherits the authority set of its parent.

# Motivation

* To improve the accessibility of the protocol's authorization mechanisms. With
  this change, authorization for every account within an ADI can be managed from
  a single place.
* To improve the feasibility of complex directory structures. Currently every
  sub-ADI and account has its own authorization set which makes these structures
  much less feasible.

# Specification


* A non-root account's authority set may be empty.
* If an account's authority set is empty, it inherits the authority set of its
  nearest ancestor with a non-empty authority set.

# Implementation

* Operations that need to know the account's authority set will recursively walk
  up the tree until they find an account with a non-empty authority set.
* Account creation transactions will leave the authority set empty by default,
  instead of copying the containing identity's authority set.
* UpdateAccountAuth will allow removing all authorities of a non-root account.

```go
func getAccountAuthoritySet(batch *database.Batch, account protocol.Account) (*protocol.AccountAuth, error) {
	// Get the account's authority set
	auth, url, err := shared.GetAccountAuthoritySet(account)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	// Fetch from some other account (e.g. page -> book)
	if auth == nil {
		account, err = batch.Account(url).Main().Get()
		if err != nil {
			return nil, errors.UnknownError.Wrap(err)
		}
		return getAccountAuthoritySet(batch, account)
	}

	// If the authority set is not empty, use that
	if len(auth.Authorities) > 0 {
		return auth, nil
	}

	// Go up a level
	url, ok := account.GetUrl().Parent()
	if !ok {
		return nil, errors.InternalError.WithFormat("authority set of root account %v is empty", account.GetUrl())
	}

	account, err = batch.Account(url).Main().Get()
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}
	return getAccountAuthoritySet(batch, account)
}
```

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).