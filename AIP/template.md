| AIP   | Title         | Status | Category   | Author                             | Created  |
| ----- | ------------- | ------ | ---------- | ---------------------------------- | -------- |
| -     | AIP Template  | Draft  | Identities | Author Name \<author@example.com\> | 20220206 |



# Summary


# Motivation


# Specification


# Implementation


# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
