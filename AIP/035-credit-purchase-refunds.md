| AIP | Title                   | Status | Category | Author                                  | Created  |
| --- | ----------------------- | ------ | -------- | --------------------------------------- | -------- |
| 011 | Credit purchase refunds | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20230901 |

# Summary

When a user makes an excessively large credit purchase, allow them to burn those
credits in such a way that staking will refund part of the ACME spent.

# Motivation

At least one user accidentally spent thousands of ACME on credits. We want to
mitigate the impact of such mistakes.

# Specification

The suggestion was considered by the governance committee and they agreed upon
the following:

- Create a credit return capability to deal with large errors.
- Only for transactions of 5,000 credits or more.
- Must be executed within 4 major blocks (48 Hours).
- A fine of 5,000 credits will be applied.
- Will use the conversion rate of the original transaction.

This will be implemented as a staking operation, since this will be sufficient
and avoids adding complexity to the core protocol:

1. The user issues a burn.
   - Must meet the criteria above.
   - Staking must be a required additional signer.
   - Must reference the original transaction.
2. Staking signs the burn, verifies it executes, and issues the ACME.

Given that staking must be a required authority, this AIP is dependent on
[AIP-008](008-additional-transaction-signers.md).

# Implementation

1. The user issues a burn with staking as a required authority.
2. Staking signs the burn and verifies it executes.
3. Staking issues the refund.

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).