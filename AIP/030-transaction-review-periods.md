| AIP | Title                      | Status | Category | Author                                  | Created  |
| --- | -------------------------- | ------ | -------- | --------------------------------------- | -------- |
| 004 | Transaction review periods | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20230604 |

# Summary

Allow the author of a transaction to specify a review period during which the
transaction will remain pending.

# Motivation

Enable more advanced authorization schemes.

# Specification

- The transaction header may specify a review period.
- If a review period is specified, authority signatures will be held back until
  the review period has expired.
- Thus the author of a transaction may force a transaction to remain pending for
  up to two weeks.
- If multiple signers (e.g. pages) of an authority (e.g. a book) reach their
  signing conditions (i.e. thresholds), the highest priority signer takes
  precedence.

# Implementation

- Transactions may specify a review period by specifying a DN block height at
  which the review period expires.
- Authority signatures are held back until a partition receives an anchor from
  the DN indicating a DN height equal to (or greater than) that expiration
  height.
- If there are multiple candidate authority signatures from different signers
  for the same authority and transaction, the candidate from the highest
  priority signer overrides all others.
- If there are multiple candidate authority signatures from the same signer for
  the same authority and transaction, the most recent candidate overrides all
  others.
- The partition will retain a list of authority signatures that are waiting for
  a review period to expire, so that they can be released once the appropriate
  anchor is received.

# Background

Originally we planned to specify the review period on the key page. However,
many signers could be involved and informing them all of when the transaction
was initiated is non-trivial, given that:

1. The signer and principal may belong to different domains;
2. Domains are prohibited from directly interacting with each other;
3. A signer may delegate to another authority, which itself may delegate.

The only obvious solution is to sending a message to all of the principal's
authorities, then to all of their delegates, then to all of *their* delegates,
etc. However that would add an inordinate degree of complexity and additional
message traffic. There are other potential solutions that may be considered in
future extensions of this functionality.

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).