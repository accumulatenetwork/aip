| AIP | Title              | Status | Category | Author                                  | Created  |
| --- | ------------------ | ------ | -------- | --------------------------------------- | -------- |
| 013 | Proxy data entries | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20240517 |

# Summary

Allow a user to submit a data transaction where the protocol or a layer 2 may
replace the content with hashes of the content without invalidating any
signatures.

# Motivation

Facilitate implementation of a data storage layer that allows users to submit
data entries that would otherwise exceed the limits of the protocol. With this
proposal such a data storage layer could be implemented in an entirely
transparent way.

# Specification

- Add proxy data entries, a new type of data entry.
- A proxy data entry has two fields, an array of content blobs and an array of
  hashes.
- One of the two fields must be populated. If both are populated, they must have
  the same length and each hash must be the SHA-256 hash of the corresponding
  content blob.
- The entry hash is calculated as a Merkle mountain range hash of the hashes
  array. If the hashes array is empty, it is populated by hashing the content
  array.
- Thus, for a given set of content blobs and hashes, signatures are valid
  regardless of which of the two fields (or both) are populated. This allows the
  protocol to reduce storage costs by only storing the hashes, and allows layer
  2 applications to transparently store the content off-chain and resubmit the
  transaction without the content.

# Implementation

https://gitlab.com/accumulatenetwork/accumulate/-/commit/d72db9f504cf405f6fa0f995735f85a92090ea8f

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).